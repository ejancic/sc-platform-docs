---
title: API Reference

language_tabs: # must be one of https://git.io/vQNgJ
  - shell: cURL

toc_footers:
  - <a href='#'>Sign Up for a Developer Key</a>
  - <a href='https://github.com/lord/slate'>Documentation Powered by Slate</a>

includes:
  - apiheaders
  - responsecodes
  - resources

search: true
---

# Introduction

Welcome to the SocialCare API.


## Data Access

_To be Defined_

## URL Structure

The pattern used for API Endpoints follows the structure outlined below.

Base-URL/resource-name?optional-query-parameters

Base-URL/resource-name/{id}

_Environment-specific Base URL Path to be defined_

Resource names are determined by which data schema is being used.

_Query Parameters to be defined_
