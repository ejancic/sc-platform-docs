# Headers

## Authentication

> To authorize, use this code:

```shell
curl "https://baseurl/patients/123"
  -H "Authorization: Bearer sc-api-key"
```

SocialCare uses API keys to allow access to the API. To request a new API key, send an email to platform@socialcare.com.

SocialCare requires the API key to be included in all API requests to the server in a header that looks like the following:

`Authorization: Bearer sc-api-key`

<aside class="notice">
You must replace <code>sc-api-key</code> with your application API key.
</aside>

## MIME Type

### Accept Header

> Sample Header for JSON

```shell
curl "https://baseurl/patients/123"
  -H "Authorization: Bearer sc-api-key"
  -H "Accept: application/json"
```

The Accept Header is required for all GET requests.  

The format specified in the Accept Header indicates how requested data will be returned (e.g. JSON, XML).

### Content Type Header

> Sample Header for JSON

```shell
curl "https://baseurl/patients/123"
  -H "Authorization: Bearer sc-api-key"
  -H "Content-Type: application/json"
```

The Content Type Header is required for all POST/PUT/PATCH requests.  

The format of the header indicates the structure of data being sent in the request (e.g. JSON, XML).

<aside class="notice">
To see a list of supported MIME types, see the Resources section
</aside>

## Data Schema

> Sample Header for JSON FHIR v3.0.1

```shell
curl "https://baseurl/patients/123"
  -H "Authorization: Bearer sc-api-key"
  -H "Content-Type: application/json"
  -H "SC-Data-Schema: fhir-v3.0.1"
```
The Data Schema header is required for all API requests.  

<aside class="notice">
To see a list of supported Data Schemas and versions, see the Resources section
</aside>

## API Version

> Sample Header for API version 1

```shell
curl "https://baseurl/patients/123"
  -H "Authorization: Bearer sc-api-key"
  -H "Content-Type: application/json"
  -H "SC-Data-Schema: fhir-v3.0.1"
  -H "SC-API-Version: v1"
```
The API Version header can be included in all API requests.  

<aside class="success">
The current API version is v1.
</aside>
