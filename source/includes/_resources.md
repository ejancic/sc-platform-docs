# Resources

The table below outlines the data standards that the Platform supports along with their corresponding MIME types.

Schema | Version(s)    | Header Value   | MIME Type(s)
--------- | ------------- | ----------- | ------------
SocialCare | v1.1.0 | sc-v1.0 | JSON
FHIR | [v3.0.1](http://hl7.org/fhir/index.html) | fhir-v3.0.1 | JSON
CCDA | [v2.1](http://www.hl7.org/implement/standards/product_brief.cfm?product_id=379) | ccda-v2.1 | XML

## Patients

> Sample Request:

```shell
curl "https://baseurl/patients/123"
  -H "Authorization: Bearer sc-api-key"
  -H "Content-Type: application/json"
  -H "SC-Data-Schema: fhir-v3.0.1"
  -H "SC-API-Version: v1.0.0"
```

> Sample Response:

```javascript
[
  {
    "id": 1,
    "firstName": "John",
    "lastName": "Doe",
    "gender": "male",
    "active": true
  }
]
```

This endpoint retrieves all patients that the requestor has access to.

`GET /patients`

This endpoint retrieves the patient with the provided ID.

`GET /patients/123`

### Query Parameters

Parameter names use dot notation and should be preceded with the resource name.

`GET /patients?patient.gender=male&patient.active=true`

Parameter | Values        | Description
--------- | ------------- | -------------------
gender | male, female | Filter patients by gender
active | true, false | Filter patients by active status
