# Response Codes

## Success Codes

The SocialCare API uses the following response codes for a request that has completed successfully.

Error Code | Meaning
---------- | -------
200 | Successful GET
201 | Successful POST
204 | Successful PUT/ PATCH/ DELETE

## Error Codes

The SocialCare API uses the following response codes:

Error Code | Meaning
---------- | -------
400 | Bad Request -- Invalid format or data has been provided for a POST/PUT/PATCH
401 | Unauthorized -- Your API key is wrong
403 | Forbidden -- The data requested is hidden for the API key provided
404 | Not Found -- The specified record could not be found
405 | Method Not Allowed -- You tried to access a record with an invalid method
406 | Not Acceptable -- An invalid value was submitted in the Accept Header
415 | Not Supported -- An invalid value was provided as the Content-Type Header
500 | Internal Server Error -- We had a problem with our server. Try again later
502 | Service Unavailable -- We're temporarily offline for maintenance. Try again later
